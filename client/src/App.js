import AirportsDataLoader from "./provider/AirportsDataLoader";
import HomeView from "./views/Home";

function App() {
  return (
    <div className="App">
      <AirportsDataLoader>
        <HomeView/>
      </AirportsDataLoader>
    </div>
  );
}

export default App;
