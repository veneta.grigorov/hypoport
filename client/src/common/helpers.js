const isPropValuesEqual = (subject, target, propNames) =>
propNames.every(propName => subject[propName] === target[propName]);

export const getUniqueItemsByProperties = (items, propNames) => 
items.filter((item, index, array) =>
  index === array.findIndex(foundItem => isPropValuesEqual(foundItem, item, propNames))
);