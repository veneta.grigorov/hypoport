const createBookingValidator = {
    firstName: (value) => {
      if (!value) {
        return false;
      }
      if (typeof value !== 'string' || value.length < 1 || value.length > 30) {
        return false;
      }
      return true;
    },
    lastName: (value) => {
      if (!value) {
        return false;
      }
      if (typeof value !== 'string' || value.length < 1 || value.length > 30) {
        return false;
      }
      return true;
    },
    departureAirportId: (value, airportIDs, arrivalAirportId) => {
      if (!value) {
        return false;
      }
      if (!airportIDs.includes(Number(value))) {
        return false;
      }
      if (arrivalAirportId) {
        if (Number(value) === Number(arrivalAirportId)) {
          return false;
        }
      }
      return true;
    },
    arrivalAirportId: (value, airportIDs, departureAirportId) => {
      if (!value) {
        return false;
      }
      if (!airportIDs.includes(Number(value))) {
        return false;
      }
      if (departureAirportId) {
        if (Number(value) === Number(departureAirportId)) {
          return false;
        }
      }
      return true;
    },
    departureDate: (value, returnDateValue) => {
      if (!value) {
        return false;
      }
      if (typeof value !== 'string') {
        return false;
      }
      const now = new Date();
      const departureDate = new Date(value);
      if (now > departureDate) {
        return false;
      }
      if (returnDateValue) {
        const returnDate = new Date(returnDateValue);
        if (returnDate < departureDate) {
          return false;
        }
      }
      return true;
    },
    returnDate: (value, departureDateValue) => {
      if (!value) {
        return false;
      }
      if (typeof value !== 'string') {
        return false;
      }
      const now = new Date();
      const returnDate = new Date(value);
      if (now > returnDate) {
        return false;
      }
      if (departureDateValue) {
        const departureDate = new Date(departureDateValue);
        if (returnDate < departureDate) {
          return false;
        }
      }
      return true;
    },
  };
  
  export default createBookingValidator;

  // const createBookingValidator = {
  //   firstName: (value) => {
  //     if (!value) {
  //       return 'First name is required';
  //     }
  //     if (typeof value !== 'string' || value.length < 1 || value.length > 30) {
  //       return 'First name should be 1-30 characters long';
  //     }
  //     return null;
  //   },
  //   lastName: (value) => {
  //     if (!value) {
  //       return 'Last name is required';
  //     }
  //     if (typeof value !== 'string' || value.length < 1 || value.length > 30) {
  //       return 'Last name should be 1-30 characters long';
  //     }
  //     return null;
  //   },
  //   departureAirportId: (value, airportIDs, arrivalAirportId) => {
  //     if (!value) {
  //       return 'Airport ID is required';
  //     }
  //     if (typeof value !== 'number') {
  //       return 'Airport ID should be a number';
  //     }
  //     if (!airportIDs.includes(value)) {
  //       return 'Selected airport does not exist.'
  //     }
  //     if (value === arrivalAirportId) {
  //       return 'Departure airport must be different from arrival airport'
  //     }
  //     return null;
  //   },
  //   arrivalAirportId: (value, airportIDs, departureAirportId) => {
  //     if (!value) {
  //       return 'Airport ID is required';
  //     }
  //     if (typeof value !== 'number') {
  //       return 'Airport ID should be a number';
  //     }
  //     if (!airportIDs.includes(value)) {
  //       return 'Selected airport does not exist.'
  //     }
  //     if (departureAirportId) {
  //       if (value === departureAirportId) {
  //         return 'Arrival airport must be different from departure airport'
  //       }
  //     }
  //     return null;
  //   },
  //   departureDate: (value, returnDateValue) => {
  //     if (!value) {
  //       return 'Departure date is required';
  //     }
  //     if (typeof value !== 'string') {
  //       return 'Departure date should be a string';
  //     }
  //     const now = new Date();
  //     const departureDate = new Date(value);
  //     if (now > departureDate) {
  //       return 'Departure date should be in the future';
  //     }
  //     if (returnDateValue) {
  //       const returnDate = new Date(returnDateValue);
  //       if (returnDate < departureDate) {
  //         return 'Departure date should be before return date'
  //       }
  //     }
  //     return null;
  //   },
  //   returnDate: (value, departureDateValue) => {
  //     if (!value) {
  //       return 'Arrival date is required';
  //     }
  //     if (typeof value !== 'string') {
  //       return 'Arrival date should be a string';
  //     }
  //     const now = new Date();
  //     const returnDate = new Date(value);
  //     if (now > returnDate) {
  //       return 'Arrival date should be in the future';
  //     }
  //     if (departureDateValue) {
  //       const departureDate = new Date(departureDateValue);
  //       if (returnDate < departureDate) {
  //         return 'Return date should be before return date'
  //       }
  //     }
  //     return null;
  //   },
  // };
  
  // export default createBookingValidator;