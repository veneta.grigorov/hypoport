import moment from 'moment';
import { useContext } from 'react';
import { momentFullTime } from '../../common/misc-constants';
import AirportsContext from '../../context/AirportsContext';
import { deleteBooking } from '../../provider/requests';
import "./SingleBooking.css";

const SingleBooking = ({ booking, bookings, setBookings }) => {
  const { airportsState } = useContext(AirportsContext);

  const departureAirportTitle = airportsState.airports?.get(booking.departureAirportId).title;
  const arrivalAirportTitle = airportsState.airports?.get(booking.arrivalAirportId).title;

  const handleDeleteBooking = () => {
    const index = bookings.list.findIndex((element) => element.id === booking.id);
    const newList = [...bookings.list]
    newList.splice(index, 1);
    const newTotalCount = bookings.totalCount--;
    setBookings({
      list: newList,
      totalCount: newTotalCount
    });
    deleteBooking(booking.id)
    .then((res) => {
      // console.log(res);
    });
  }
  
  return (
    <div className="booking">
      <div className="bookingInfo">
        <p><span className="fieldName">Name: </span>{`${booking.firstName} ${booking.lastName}`}</p>
        <p><span className="fieldName">Departure Date: </span>{moment(booking.departureDate).format(momentFullTime)}</p>
        <p><span className="fieldName">Departure Airport: </span>{departureAirportTitle}</p>
        <p><span className="fieldName">Arrival Airport: </span>{arrivalAirportTitle}</p>
        <p><span className="fieldName">Return Date: </span>{moment(booking.returnDate).format(momentFullTime)}</p>
      </div>
      <button className="deleteButton" onClick={handleDeleteBooking}>delete</button>
    </div>
  );
};

export default SingleBooking;