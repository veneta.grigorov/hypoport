import SingleBooking from "./SingleBooking";
import './Bookings.css';

const Bookings = ({ bookings, setBookings }) => {

  return (
    <div className="bookings">
      {Object.keys(bookings).length === 0
        ? <p>No bookings to show!</p>
        : <>
            {bookings.list.map((booking) => (
              <SingleBooking key={Math.random()} booking={booking} bookings={bookings} setBookings={setBookings} />
            ))}
          </>
      }
    </div>
  );
};

export default Bookings;
