import { useContext, useState } from "react";
import AirportsContext from "../../context/AirportsContext";
import { createBooking } from "../../provider/requests";
import createBookingValidator from "../../validators/createBookingSchema";
import "./Form.css";

const Form = ({ bookings, setBookings }) => {
  const { airportsState } = useContext(AirportsContext);
  const airportIDs = airportsState.airportsArray.map((airport) => airport.id);

  const [formData, setFormData] = useState({
    firstName: {
      name: "firstName",
      value: "",
      isValid: false,
    },
    lastName: {
      name: "lastName",
      value: "",
      isValid: false
    },
    departureAirportId: {
      name: "departureAirport",
      value: "",
      isValid: false
    },
    arrivalAirportId: {
      name: "arrivalAirport",
      value: "",
      isValid: false
    },
    departureDate: {
      name: "departureDate",
      value: "",
      isValid: false
    },
    returnDate: {
      name: "returnDate",
      value: "",
      isValid: false
    },
  });

  const handleChangeFirstName = (e) => {
    setFormData({
      ...formData,
      firstName: {
        ...formData.firstName,
        isValid: createBookingValidator.firstName(e.target.value),
        value: e.target.value,
      },
    });
  };

  const handleChangeLastName = (e) => {
    setFormData({
      ...formData,
      lastName: {
        ...formData.lastName,
        isValid: createBookingValidator.lastName(e.target.value),
        value: e.target.value,
      },
    });
  };

  const handleChangeDepartureAirport = (e) => {
    setFormData({
      ...formData,
      departureAirportId: {
        ...formData.departureAirportId,
        isValid: createBookingValidator.departureAirportId(e.target.value, airportIDs, formData.arrivalAirportId.value),
        value: Number(e.target.value),
      },
    });
  };

  const handleChangeArrivalAirport = (e) => {
    setFormData({
      ...formData,
      arrivalAirportId: {
        ...formData.arrivalAirportId,
        isValid: createBookingValidator.arrivalAirportId(e.target.value, airportIDs, formData.departureAirportId.value),
        value: Number(e.target.value),
      },
    });
  };

  const handleChangeDepartureDate = (e) => {
    setFormData({
      ...formData,
      departureDate: {
        ...formData.departureDate,
        isValid: createBookingValidator.departureDate(e.target.value, formData.returnDate.value),
        value: e.target.value,
      },
    });
  };

  const handleChangeReturnDate = (e) => {
    setFormData({
      ...formData,
      returnDate: {
        ...formData.returnDate,
        isValid: createBookingValidator.returnDate(e.target.value, formData.departureDate.value),
        value: e.target.value,
      },
    });
  };

  const handleSubmitBooking = (e) => {
    e.preventDefault();
    const bookingInfo = {
      firstName: formData.firstName.value,
      lastName: formData.lastName.value,
      departureAirportId: formData.departureAirportId.value,
      arrivalAirportId: formData.arrivalAirportId.value,
      departureDate: formData.departureDate.value,
      returnDate: formData.returnDate.value,
    }
    createBooking(bookingInfo)
    .then((res) => {
      setFormData({
        firstName: {
          ...formData.firstName,
          value: "",
          isValid: false
        },
        lastName: {
          ...formData.lastName,
          value: "",
          isValid: false
        },
        departureAirportId: {
          ...formData.departureAirportId,
          value: "",
          isValid: false
        },
        arrivalAirportId: {
          ...formData.arrivalAirportId,
          value: "",
          isValid: false
        },
        departureDate: {
          ...formData.departureDate,
          value: "",
          isValid: false
        },
        returnDate: {
          ...formData.returnDate,
          value: "",
          isValid: false
        }
      })
      const newBookingsList = [...bookings.list];
      const newBooking = res;
      newBookingsList.unshift(newBooking);
      const newTotalCount = bookings.totalCount++;
      setBookings({
        list: newBookingsList,
        totalCount: newTotalCount
      })
    });
  };

  return (
    <form autoComplete="off" onSubmit={handleSubmitBooking}>
      <div className="bookingForm" >
        <div className="field">
          <label className="label">First Name </label>
          <input className="input"
            required
            type="text" 
            id="firstName" 
            name="firstName"
            value={formData.firstName.value}
            onChange={handleChangeFirstName}
          />
          <br />
        </div>

        <div className="field">
          <label className="label">Last Name </label>
          <input className="input"
            required 
            type="text" 
            id="lastName" 
            name="lastName"
            value={formData.lastName.value}
            onChange={handleChangeLastName}
          />
          <br />
        </div> 

        <div className="field">
          <label className="label">Departure Airport </label>
          <select className="input"
            required
            type="text" 
            id="departureAirport" 
            name="departureAirport"
            value={formData.departureAirportId.value}
            onChange={handleChangeDepartureAirport}
          >
            <option value=""> </option>
            {airportsState?.airportsArray.map((airport) => (
              <option key={Math.random()} value={airport.id}>{airport.title}</option>
            ))}
          </select>
          <br />
        </div>

        <div className="field">
          <label className="label">Arrival Airport </label>
          <select className="input"
            required
            type="text" 
            id="arrivalAirport" 
            name="arrivalAirport"
            value={formData.arrivalAirportId.value}
            onChange={handleChangeArrivalAirport}
          >
            <option value=""> </option>
            {airportsState?.airportsArray.map((airport) => (
              <option key={Math.random()} value={airport.id}>{airport.title}</option>
            ))}
          </select>
          <br />
        </div>

        <div className="field">
          <label className="label">Departure Date </label>
          <input className="input"
            required 
            type="date" 
            id="departureDate" 
            name="departureDate"
            value={formData.departureDate.value}
            onChange={handleChangeDepartureDate}
          />
          <br />
        </div>
        
        <div className="field">
          <label className="label">Return Date </label>
          <input className="input"
            required
            type="date" 
            id="returnDate" 
            name="returnDate"
            value={formData.returnDate.value}
            onChange={handleChangeReturnDate}
          />
          <br />
        </div>

        {(!formData.firstName.isValid ||
          !formData.lastName.isValid ||
          !formData.departureAirportId.isValid ||
          !formData.arrivalAirportId.isValid ||
          !formData.departureDate.isValid ||
          !formData.returnDate.isValid)
          ? <input className="submit"
              type="submit" 
              value="Submit" 
              disabled
            />
          : <input className="submit"
              type="submit" 
              value="Submit" 
            />
        }
      </div>
    </form>
  );
};

export default Form;
