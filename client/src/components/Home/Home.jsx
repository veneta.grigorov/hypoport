import { useState, useEffect } from "react";
import { getUniqueItemsByProperties } from "../../common/helpers";
import { getBookings } from "../../provider/requests";
import Bookings from "../Bookings/Bookings";
import Form from "../Form/Form";
import "./Home.css";

const Home = () => {
  const [reloadBookings, setReloadBookings] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [noMoreBookingsToFetch, setNoMoreBookingsToFetch] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(window.scrollY);
  const [bookings, setBookings] = useState({ list: [], totalCount: 0 });
  const [pageIndex, setPageIndex] = useState(0);

  const increaseDisplayedCourses = () => {
    setPageIndex((prev) => prev + 1);
    setReloadBookings(true);
  }
  window.onscroll = function(event) {
    event.preventDefault();
    if ((window.innerHeight + Math.ceil(window.pageYOffset)) >= document.body.offsetHeight) {
      setScrollPosition(window.scrollY);
      if (!noMoreBookingsToFetch && !isLoading) {
        increaseDisplayedCourses();
      }
    }
  }

  useEffect(() => {
    async function fetchBookings() {
      setIsLoading(true);
      const fetchedBookings = await getBookings(pageIndex);
      if (fetchedBookings.list.length === 0) {
        setNoMoreBookingsToFetch(true);
      } else {
        const newBookingsList = [...bookings.list, ...fetchedBookings.list];
        const filteredBookingsList = getUniqueItemsByProperties(newBookingsList, ['id']);
        const newBookings = {list:filteredBookingsList, totalCount:Number(fetchedBookings.totalCount)};
        setBookings(newBookings);
      }
      setIsLoading(false);
      window.scrollTo(0, scrollPosition);
    }
    if (reloadBookings) {
      fetchBookings();
      setReloadBookings(false);
    }
  }, [reloadBookings, bookings, pageIndex, scrollPosition]);

  return (
    <div className="home">
      <h1 className="title">Hypoport Airlines</h1>
      <h3>Create a new booking</h3>
      <Form bookings={bookings} setBookings={setBookings}/>
      <h3>Bookings</h3>
      {isLoading
        ? <p>Bookings are loading...</p>
        : <Bookings bookings={bookings} setBookings={setBookings}/>
      }
    </div>
  );
};

export default Home;
