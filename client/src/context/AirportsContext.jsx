import { createContext } from "react";

const AirportsContext = createContext({
  airportsState: {
    airports: {},
    airportsArray: [],
  },
  setAirportsState: () => {},
});

export default AirportsContext;
