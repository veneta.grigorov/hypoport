import { useEffect, useState } from "react";
import Loader from "../components/Loader/Loader";
import AirportsContext from "../context/AirportsContext";
import { getAirports } from "./requests";

const AirportsDataLoader = (props) => {
  const [airportsState, setAirportsState] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  
  useEffect(() => {
    async function fetchData() {
      setIsLoading(true);

      const airportsArray = await getAirports();
      const airports = new Map();
      airportsArray.forEach((airport) => airports.set(airport.id, airport));

      setAirportsState({ 
        airports,
        airportsArray
      });
      setIsLoading(false);
    }
    fetchData();
  }, [setAirportsState]);

  return (
    <AirportsContext.Provider value={{ airportsState, setAirportsState }}>
      {isLoading
        ? <Loader/>
        : <>{props.children}</>
      }
    </AirportsContext.Provider>
  )
};


export default AirportsDataLoader;