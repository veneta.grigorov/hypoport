import { API_URL, API_KEY } from "../common/api-constants";

export const getAirports = async () => {
  const bookings = await fetch(`${API_URL}/airports?authToken=${API_KEY}`, {
    method: "GET",
  })
    .then((res) => res.json())
    .catch((err) => {
      throw new Error(err)
    });

  return bookings;
};

export const getBookings = async (pageIndex) => {
  const bookings = await fetch(`${API_URL}/bookings?authToken=${API_KEY}&pageIndex=${pageIndex}`, {
    method: "GET",
  })
    .then((res) => res.json())
    .catch((err) => {
      throw new Error(err)
    });

  return bookings;
};

// export const getAllBookings = async (pageSize) => {
//   const bookings = await fetch(`${API_URL}/bookings?authToken=${API_KEY}&pageIndex=0&pageSize=${pageSize}`, {
//     method: "GET",
//   })
//     .then((res) => res.json())
//     .catch((error) => console.log(error));

//   return bookings;
// };

export const getBooking = async (bookingId) => {
  const booking = await fetch(`${API_URL}/bookings/${bookingId}?authToken=${API_KEY}`, {
    method: "GET",
  })
    .then((res) => res.json())
    .catch((err) => {
      throw new Error(err)
    });

  return booking;
};


export const createBooking = async (bookingInfo) => {
  const booking = await fetch(`${API_URL}/bookings/create?authToken=${API_KEY}`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      ...bookingInfo
    })
  })
    .then((res) => res.json())
    .catch((err) => {
      throw new Error(err)
    });

  return booking;
};

export const deleteBooking = async (bookingId) => {
  const booking = await fetch(`${API_URL}/bookings/delete/${bookingId}?authToken=${API_KEY}`, {
    method: "DELETE",
  })
    // .then((res) => res.json())
    .catch((err) => {
      throw new Error(err)
    });

  return booking;
};